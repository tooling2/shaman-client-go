package shaman

/*
Shaman API specs

| Route | Description | Payload | Output |
| --- | --- | --- | --- |
| **POST** /records | Adds the domain and full record | json domain object | json domain object |
| **PUT** /records | Update all domains and records (replaces all) | json array of domain objects | json array of domain objects |
| **GET** /records | Returns a list of domains we have records for | nil | string array of domains |
| **PUT** /records/{domain} | Update domain's records (replaces all) | json domain object | json domain object |
| **GET** /records/{domain} | Returns the records for that domain | nil | json domain object |
| **DELETE** /records/{domain} | Delete a domain | nil | success message |

**note:** The API requires a token to be passed for authentication by default and is configurable at server start (`--token`). The token is passed in as a custom header: `X-AUTH-TOKEN`.

More info here: https://github.com/nanopack/shaman

This work is based on the one from CloudFlare, thanks to them.

Author: Juan Matias KungFu de la Camara Beovide <juanmatias@gmail.com>
Company: 3XM Group: https://www.3xmgroup.com/

March 2020
This code is COVID-19 free, but use it at your own risk.

*/

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

    cmp "github.com/google/go-cmp/cmp"

)

var (
	// mux is the HTTP request multiplexer used with the test server.
	mux *http.ServeMux

	// client is the API client being tested
	client *API

	// server is a test HTTP server used to provide mock API responses
	server *httptest.Server
)


func setup() {
	// test server
	mux = http.NewServeMux()
	server = httptest.NewServer(mux)

	client, _ = NewWithAPIToken("secret", server.URL)
}

func teardown() {
	server.Close()
}


func TestGetDomains(t *testing.T) {
	setup()
	defer teardown()

	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "GET" {
            t.Errorf("Expected method 'GET'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `["nanopack.io.","www.3xmgroup.com.", "juanmatiasdelacamara.wordpress.com."]`)
	}

	// `HandleFunc` doesn't handle query parameters so we just need to
	// handle the `/records` endpoint instead.
	mux.HandleFunc("/records", handler)

    expected := DomainsResponse{"nanopack.io.", "www.3xmgroup.com.", "juanmatiasdelacamara.wordpress.com."}
	actual, err := client.GetDomains()
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}


func TestGetRecords (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "GET" {
            t.Errorf("Expected method 'GET'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `{
                  "domain": "nanopack.io.",
                  "records": [
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.1"
                    },
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.2"
                    }
                  ]
                }`)
	}

	mux.HandleFunc(fmt.Sprintf("/records/%v", domain), handler)

    dnsrecords := make([]DNSRecordResponse,2)
    dnsrecords[0] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.1",
                    }
    dnsrecords[1] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.2",
                    }
    expected := DomainResponse{
                  Domain: domain,
                  Records: dnsrecords,
                  }
	actual, err := client.GetRecords(domain)
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}



func TestDeleteDomain (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "DELETE" {
            t.Errorf("Expected method 'DELETE'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `{
                  "msg": "success"
                }`)
	}

	mux.HandleFunc(fmt.Sprintf("/records/%v", domain), handler)

    expected := MsgResponse{
                  "success",
                  }
	actual, err := client.DeleteDomain(domain)
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}

func TestCreateDomain (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "POST" {
            t.Errorf("Expected method 'POST'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `{
                  "domain": "nanopack.io.",
                  "records": []
                }`)
	}

	mux.HandleFunc("/records", handler)

    dnsrecords := []DNSRecordResponse{}
    expected := DomainResponse{
                  Domain: domain,
                  Records: dnsrecords,
                  }
	actual, err := client.CreateDomain(domain)
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}

func TestCreateRecords (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "POST" {
            t.Errorf("Expected method 'POST'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `{
                  "domain": "nanopack.io.",
                  "records": [
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.1"
                    },
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.2"
                    }
                  ]
                }`)
	}

	mux.HandleFunc("/records", handler)

    // Data to send
    dnsrecords := make([]DNSRecord,2)
    dnsrecords[0] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.1",
                    }
    dnsrecords[1] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.2",
                    }
    // Data to compare to the response
    dnsrecordsR := make([]DNSRecordResponse,2)
    dnsrecordsR[0] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.1",
                    }
    dnsrecordsR[1] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.2",
                    }
    expected := DomainResponse{
                  Domain: domain,
                  Records: dnsrecordsR,
                  }
	actual, err := client.CreateRecords(domain, dnsrecords)
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}

func TestCreateDomainWithRecords (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "POST" {
            t.Errorf("Expected method 'POST'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `{
                  "domain": "nanopack.io.",
                  "records": [
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.1"
                    },
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.2"
                    }
                  ]
                }`)
	}

	mux.HandleFunc("/records", handler)

    // Data to send
    dnsrecords := make([]DNSRecord,2)
    dnsrecords[0] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.1",
                    }
    dnsrecords[1] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.2",
                    }
    // Data to compare to the response
    dnsrecordsR := make([]DNSRecordResponse,2)
    dnsrecordsR[0] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.1",
                    }
    dnsrecordsR[1] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.2",
                    }
    expected := DomainResponse{
                  Domain: domain,
                  Records: dnsrecordsR,
                  }
	actual, err := client.CreateDomainWithRecords(domain, dnsrecords)
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}

func TestReplaceDomains (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "PUT" {
            t.Errorf("Expected method 'PUT'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `[{
                  "domain": "nanopack.io.",
                  "records": [
                  ]
                }]`)
	}

	mux.HandleFunc("/records", handler)

    // Data to send
    domains := []string{domain}
    // Data to compare to the response
    dnsrecordsR := []DNSRecordResponse{}
    dr := DomainResponse{
                  Domain: domain,
                  Records: dnsrecordsR,
                  }
    expected := []DomainResponse{dr}
	actual, err := client.ReplaceDomains(domains)
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}

func TestReplaceDomainsWithRecords (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "PUT" {
            t.Errorf("Expected method 'PUT'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `[{
                  "domain": "nanopack.io.",
                  "records": [
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.4"
                    },
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.5"
                    }
                  ]
                }]`)
	}

	mux.HandleFunc("/records", handler)

    // Data to send
    dnsrecords := make([]DNSRecord,2)
    dnsrecords[0] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.4",
                    }
    dnsrecords[1] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.5",
                    }
    payload := Domain{
                  Domain: domain,
                  Records: dnsrecords,
                  }
    // Data to compare to the response
    dnsrecordsR := make([]DNSRecordResponse,2)
    dnsrecordsR[0] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.4",
                    }
    dnsrecordsR[1] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.5",
                    }
    dr := DomainResponse{
                  Domain: domain,
                  Records: dnsrecordsR,
                  }
    expected := []DomainResponse{dr}
	actual, err := client.ReplaceDomainsWithRecords([]Domain{payload})
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}

func TestReplaceDomainRecords (t *testing.T) {
	setup()
	defer teardown()

    domain := "nanopack.io."
	handler := func(w http.ResponseWriter, r *http.Request) {
        if r.Method != "PUT" {
            t.Errorf("Expected method 'PUT'. got %s", r.Method)
        }
		w.Header().Set("content-type", "application/json")
		fmt.Fprintf(w, `{
                  "domain": "nanopack.io.",
                  "records": [
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.4"
                    },
                    {
                      "ttl": 60,
                      "class": "IN",
                      "type": "A",
                      "address": "127.0.0.5"
                    }
                  ]
                }`)
	}

	mux.HandleFunc(fmt.Sprintf("/records/%v", domain), handler)

    // Data to send
    dnsrecords := make([]DNSRecord,2)
    dnsrecords[0] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.4",
                    }
    dnsrecords[1] = DNSRecord{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.5",
                    }
    // Data to compare to the response
    dnsrecordsR := make([]DNSRecordResponse,2)
    dnsrecordsR[0] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.4",
                    }
    dnsrecordsR[1] = DNSRecordResponse{
                      TTL: 60,
                      Class: "IN",
                      Type: "A",
                      Address: "127.0.0.5",
                    }
    expected := DomainResponse{
                  Domain: domain,
                  Records: dnsrecordsR,
                  }
	actual, err := client.ReplaceDomainRecords(domain, dnsrecords)
    if err != nil {
        t.Errorf("Error: %v",err)
    }else{
        if ! cmp.Equal(expected, actual) {
            t.Errorf("Expected: '%v', got '%v'",expected, actual)
        }
    }

}

