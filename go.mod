module gitlab.com/tooling2/shaman-client-go

go 1.13

require (
	github.com/google/go-cmp v0.4.0
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.5.1
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
)
