package shaman

/*
Shaman API specs

| Route | Description | Payload | Output |
| --- | --- | --- | --- |
| **POST** /records | Adds the domain and full record | json domain object | json domain object |
| **PUT** /records | Update all domains and records (replaces all) | json array of domain objects | json array of domain objects |
| **GET** /records | Returns a list of domains we have records for | nil | string array of domains |
| **PUT** /records/{domain} | Update domain's records (replaces all) | json domain object | json domain object |
| **GET** /records/{domain} | Returns the records for that domain | nil | json domain object |
| **DELETE** /records/{domain} | Delete a domain | nil | success message |

**note:** The API requires a token to be passed for authentication by default and is configurable at server start (`--token`). The token is passed in as a custom header: `X-AUTH-TOKEN`.

More info here: https://github.com/nanopack/shaman

This work is based on the one from CloudFlare, thanks to them.

Author: Juan Matias KungFu de la Camara Beovide <juanmatias@gmail.com>
Company: 3XM Group: https://www.3xmgroup.com/

March 2020
This code is COVID-19 free, but use it at your own risk.

*/

import (
	"context"
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"strings"
	"time"

	"fmt"
	"crypto/tls"

	"github.com/pkg/errors"
	"golang.org/x/time/rate"
)

// ***************************************************************************************************
// Consts, structs and interfaces

const apiURL = "https://172.0.0.1:1632"

const (
	// AuthKeyEmail specifies that we should authenticate with API key and email address
	AuthToken = 1 << iota
	// AuthUserService specifies that we should authenticate with a User-Service key
	AuthUserService
	// AuthToken specifies that we should authenticate with an API Token
	AuthKeyEmail 
)

// **************************
// Domain and Records structs
/* Fields:

    domain: Domain name to resolve
    records: Array of address records
        ttl: Seconds a client should cache for
        class: Record class
        type: Record type
            A - Address record
            CNAME - Canonical name record
            MX - Mail exchange record
            Many more (https://en.wikipedia.org/wiki/List_of_DNS_record_types) - may or may not work as is
        address: Address domain resolves to
            note: Special rules apply in some cases. E.g. MX records require a number "10 mail.google.com"
*/
type DNSRecord struct {
	TTL uint8 `json:"ttl"`
	Class string `json:"class"`
	Type	string `json:"type"`
	Address	string `json:"address"`
}

type Domain struct {
	Domain	string `json:"domain"`
	Records	[]DNSRecord `json:"records"`
}

type DNSRecordResponse struct {
	TTL uint8 `json:"ttl"`
	Class string `json:"class"`
	Type	string `json:"type"`
	Address	string `json:"address"`
}

type DomainResponse struct {
	Domain	string `json:"domain"`
	Records	[]DNSRecordResponse `json:"records"`
}

type DomainsResponse []string

type MsgResponse struct {
	Msg string
}

// API holds the configuration for the current API client. A client should not
// be modified concurrently.
type API struct {
	APIKey            string
	APIEmail          string
	APIUserServiceKey string
	APIToken          string
	BaseURL           string
	AccountID         string
	UserAgent         string
	headers           http.Header
	httpClient        *http.Client
	authType          int
	rateLimiter       *rate.Limiter
	retryPolicy       RetryPolicy
	logger            Logger
	Insecure          bool
}

// Logger defines the interface this library needs to use logging
// This is a subset of the methods implemented in the log package
type Logger interface {
	Printf(format string, v ...interface{})
}

// RetryPolicy specifies number of retries and min/max retry delays
// This config is used when the client exponentially backs off after errored requests
type RetryPolicy struct {
	MaxRetries    int
	MinRetryDelay time.Duration
	MaxRetryDelay time.Duration
}

// ***************************************************************************************************
// Creating the client

func newClient() (*API, error) {
	silentLogger := log.New(ioutil.Discard, "", log.LstdFlags)

	api := &API{
		BaseURL:     apiURL,
		headers:     make(http.Header),
		rateLimiter: rate.NewLimiter(rate.Limit(4), 1), // 4rps equates to default api limit (1200 req/5 min)
		retryPolicy: RetryPolicy{
			MaxRetries:    3,
			MinRetryDelay: time.Duration(1) * time.Second,
			MaxRetryDelay: time.Duration(30) * time.Second,
		},
		logger: silentLogger,
		Insecure: true,
	}

	if api.Insecure == true{
	        // This transport is what's causing unclosed connections.
	        tr := &http.Transport{
	            TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	        }
	        hc := &http.Client{Timeout: 2 * time.Second, Transport: tr}
		api.httpClient = hc // http.DefaultClient
	}else{
		api.httpClient = http.DefaultClient
	}

	return api, nil
}


func NewWithAPIToken(token string, baseURL string) (*API, error) {
	if token == "" {
		return nil, errors.New(errEmptyAPIToken)
	}

	api, err := newClient()
	if err != nil {
		return nil, err
	}

	api.APIToken = token
	api.authType = AuthToken

    if baseURL != "" {
        api.BaseURL = baseURL
    }

	return api, nil
}

// ***************************************************************************************************
// General calls


func (api *API) GetDomains() (DomainsResponse, error) {
	var r DomainsResponse
	res, err := api.makeRequest("GET", "/records", nil)
	if err != nil {
		return DomainsResponse{}, errors.Wrap(err, errMakeRequestError)
	}

	err = json.Unmarshal(res, &r)
	if err != nil {
		return DomainsResponse{}, errors.Wrap(err, errUnmarshalError)
	}

	return r, nil
}

func (api *API) GetRecords(domainName string) (DomainResponse, error) {
	var r DomainResponse
	uri := fmt.Sprintf("/records/%v",domainName)

	res, err := api.makeRequest("GET", uri, nil)
	if err != nil {
		return DomainResponse{}, errors.Wrap(err, errMakeRequestError)
	}

	err = json.Unmarshal(res, &r)
	if err != nil {
		return DomainResponse{}, errors.Wrap(err, errUnmarshalError)
	}

	return r, nil
}

func (api *API) DeleteDomain(domainName string) (MsgResponse, error) {
	var r MsgResponse
	uri := fmt.Sprintf("/records/%v",domainName)

	res, err := api.makeRequest("DELETE", uri, nil)
	if err != nil {
		return MsgResponse{}, errors.Wrap(err, errMakeRequestError)
	}

	err = json.Unmarshal(res, &r)
	if err != nil {
		return MsgResponse{}, errors.Wrap(err, errUnmarshalError)
	}

	return r, nil
}


func (api *API) CreateDomain(name string) (DomainResponse, error) {
	return api.CreateDomainWithRecords(name, []DNSRecord{})
}

func (api *API) CreateRecords(domainName string, records []DNSRecord) (DomainResponse, error) {
	return api.CreateDomainWithRecords(domainName, records)
}

func (api *API) CreateDomainWithRecords(domainName string, records []DNSRecord) (DomainResponse, error) {
	var newdomain Domain
	newdomain.Domain = domainName
	newdomain.Records = records

	res, err := api.makeRequest("POST", "/records", newdomain)
	if err != nil {
		return DomainResponse{}, errors.Wrap(err, errMakeRequestError)
	}

	var r DomainResponse
	err = json.Unmarshal(res, &r)
	if err != nil {
		return DomainResponse{}, errors.Wrap(err, errUnmarshalError)
	}
	return r, nil
}

// Replace* functions will replace all the domains in the database with the received ones and their records

func (api *API) ReplaceDomains(domainNames []string) ([]DomainResponse, error) {
	replacementDomains := make([]Domain, len(domainNames))
	for i := range domainNames {
		replacementDomains[i].Domain = domainNames[i]
		replacementDomains[i].Records = []DNSRecord{}
	}
	return api.ReplaceDomainsWithRecords(replacementDomains)
}

func (api *API) ReplaceDomainsWithRecords(replacementDomains []Domain) ([]DomainResponse, error) {

	res, err := api.makeRequest("PUT", "/records", replacementDomains)
	if err != nil {
		return []DomainResponse{}, errors.Wrap(err, errMakeRequestError)
	}

	var r []DomainResponse
	err = json.Unmarshal(res, &r)
	if err != nil {
		return []DomainResponse{}, errors.Wrap(err, errUnmarshalError)
	}
	return r, nil
}

func (api *API) ReplaceDomainRecords(domainName string, records []DNSRecord) (DomainResponse, error) {
	replaceDomain := Domain{}

	replaceDomain.Domain = domainName
	replaceDomain.Records = records

	uri := fmt.Sprintf("/records/%v",domainName)

	res, err := api.makeRequest("PUT", uri, replaceDomain)
	if err != nil {
		return DomainResponse{}, errors.Wrap(err, errMakeRequestError)
	}

	var r DomainResponse
	err = json.Unmarshal(res, &r)
	if err != nil {
		return DomainResponse{}, errors.Wrap(err, errUnmarshalError)
	}
	return r, nil
}


// ***************************************************************************************************
// Requests

// makeRequest makes a HTTP request and returns the body as a byte slice,
// closing it before returning. params will be serialized to JSON.
func (api *API) makeRequest(method, uri string, params interface{}) ([]byte, error) {
	return api.makeRequestWithAuthType(context.TODO(), method, uri, params, api.authType)
}

func (api *API) makeRequestWithAuthType(ctx context.Context, method, uri string, params interface{}, authType int) ([]byte, error) {
	return api.makeRequestWithAuthTypeAndHeaders(ctx, method, uri, params, authType, nil)
}

func (api *API) makeRequestWithAuthTypeAndHeaders(ctx context.Context, method, uri string, params interface{}, authType int, headers http.Header) ([]byte, error) {
	// Replace nil with a JSON object if needed
	var jsonBody []byte
	var err error

	if params != nil {
		if paramBytes, ok := params.([]byte); ok {
			jsonBody = paramBytes
		} else {
			jsonBody, err = json.Marshal(params)
			if err != nil {
				return nil, errors.Wrap(err, "error marshalling params to JSON")
			}
		}
	} else {
		jsonBody = nil
	}

	var resp *http.Response
	var respErr error
	var reqBody io.Reader
	var respBody []byte
	for i := 0; i <= api.retryPolicy.MaxRetries; i++ {
		if jsonBody != nil {
			reqBody = bytes.NewReader(jsonBody)
		}
		if i > 0 {
			// expect the backoff introduced here on errored requests to dominate the effect of rate limiting
			// don't need a random component here as the rate limiter should do something similar
			// nb time duration could truncate an arbitrary float. Since our inputs are all ints, we should be ok
			sleepDuration := time.Duration(math.Pow(2, float64(i-1)) * float64(api.retryPolicy.MinRetryDelay))

			if sleepDuration > api.retryPolicy.MaxRetryDelay {
				sleepDuration = api.retryPolicy.MaxRetryDelay
			}
			// useful to do some simple logging here, maybe introduce levels later
			api.logger.Printf("Sleeping %s before retry attempt number %d for request %s %s", sleepDuration.String(), i, method, uri)
			time.Sleep(sleepDuration)

		}
		err = api.rateLimiter.Wait(context.TODO())
		if err != nil {
			return nil, errors.Wrap(err, "Error caused by request rate limiting")
		}
		resp, respErr = api.request(ctx, method, uri, reqBody, authType, headers)

		// retry if the server is rate limiting us or if it failed
		// assumes server operations are rolled back on failure
		if respErr != nil || resp.StatusCode == http.StatusTooManyRequests || resp.StatusCode >= 500 {
			// if we got a valid http response, try to read body so we can reuse the connection
			// see https://golang.org/pkg/net/http/#Client.Do
			if respErr == nil {
				respBody, err = ioutil.ReadAll(resp.Body)
				resp.Body.Close()

				respErr = errors.Wrap(err, "could not read response body")

				api.logger.Printf("Request: %s %s got an error response %d: %s\n", method, uri, resp.StatusCode,
					strings.Replace(strings.Replace(string(respBody), "\n", "", -1), "\t", "", -1))
			} else {
				api.logger.Printf("Error performing request: %s %s : %s \n", method, uri, respErr.Error())
			}
			continue
		} else {
			respBody, err = ioutil.ReadAll(resp.Body)
			defer resp.Body.Close()
			if err != nil {
				return nil, errors.Wrap(err, "could not read response body")
			}
			break
		}
	}
	if respErr != nil {
		return nil, respErr
	}

	switch {
	case resp.StatusCode >= http.StatusOK && resp.StatusCode < http.StatusMultipleChoices:
	case resp.StatusCode == http.StatusUnauthorized:
		return nil, errors.Errorf("HTTP status %d: invalid credentials", resp.StatusCode)
	case resp.StatusCode == http.StatusForbidden:
		return nil, errors.Errorf("HTTP status %d: insufficient permissions", resp.StatusCode)
	case resp.StatusCode == http.StatusServiceUnavailable,
		resp.StatusCode == http.StatusBadGateway,
		resp.StatusCode == http.StatusGatewayTimeout,
		resp.StatusCode == 522,
		resp.StatusCode == 523,
		resp.StatusCode == 524:
		return nil, errors.Errorf("HTTP status %d: service failure", resp.StatusCode)
	// This isn't a great solution due to the way the `default` case is
	// a catch all and that the `filters/validate-expr` returns a HTTP 400
	// yet the clients need to use the HTTP body as a JSON string.
	case resp.StatusCode == 400 && strings.HasSuffix(resp.Request.URL.Path, "/filters/validate-expr"):
		return nil, errors.Errorf("%s", respBody)
	default:
		var s string
		if respBody != nil {
			s = string(respBody)
		}
		return nil, errors.Errorf("HTTP status %d: content %q", resp.StatusCode, s)
	}

	return respBody, nil
}


// request makes a HTTP request to the given API endpoint, returning the raw
// *http.Response, or an error if one occurred. The caller is responsible for
// closing the response body.
func (api *API) request(ctx context.Context, method, uri string, reqBody io.Reader, authType int, headers http.Header) (*http.Response, error) {
	req, err := http.NewRequest(method, api.BaseURL+uri, reqBody)
	if err != nil {
		return nil, errors.Wrap(err, "HTTP request creation failed")
	}
	req.WithContext(ctx)

	combinedHeaders := make(http.Header)
	copyHeader(combinedHeaders, api.headers)
	copyHeader(combinedHeaders, headers)
	req.Header = combinedHeaders

	if authType&AuthKeyEmail != 0 {
		req.Header.Set("X-Auth-Key", api.APIKey)
		req.Header.Set("X-Auth-Email", api.APIEmail)
	}
	if authType&AuthUserService != 0 {
		req.Header.Set("X-Auth-User-Service-Key", api.APIUserServiceKey)
	}
	if authType&AuthToken != 0 {
		req.Header.Set("X-AUTH-TOKEN", api.APIToken)
	}

	if api.UserAgent != "" {
		req.Header.Set("User-Agent", api.UserAgent)
	}

	if req.Header.Get("Content-Type") == "" {
		req.Header.Set("Content-Type", "application/json")
	}

	resp, err := api.httpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "HTTP request failed")
	}

	return resp, nil
}

// copyHeader copies all headers for `source` and sets them on `target`.
// based on https://godoc.org/github.com/golang/gddo/httputil/header#Copy
func copyHeader(target, source http.Header) {
	for k, vs := range source {
		target[k] = vs
	}
}

